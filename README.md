# atof

"The function […] takes as many characters as possible that are valid following a syntax resembling that of floating point literals (see below), and interprets them as a numerical value."
