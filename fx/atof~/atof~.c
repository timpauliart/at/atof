#include "m_pd.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>

static t_class *atof_tilde_class;

typedef struct _atof_tilde
{
        t_object x_obj; // object
        t_sample x_f;
        t_outlet *x_out; // audio outlet
} t_atof_tilde;


void *atof_tilde_new()
{
        t_atof_tilde *x = (t_atof_tilde *)pd_new(atof_tilde_class);
        x->x_out = outlet_new(&x->x_obj, gensym("signal"));
        return (void*) x;
}

t_int *atof_tilde_perform(t_int *w)
{
        // unpack data
        t_sample *in = (t_sample *)(w[2]);
        t_sample *out = (t_sample *)(w[3]);
        int n = (int)(w[4]);

        int floatlen = sizeof(t_sample);
        char str[floatlen + 1]; // 1 float is 4 chars
        str[floatlen] = '\0';
        // process the whole buffer
        for (int i = 0; i < n; ++i) {
                memcpy(str, &(in[i]), floatlen); // convert float to str
                float val = atof(str);  // convert str to float
                val = val / pow(10, floatlen); // scale float
                out[i] = (t_sample) val; // output
        }
        // goto next buffer
        return (w+5);
}

void atof_tilde_dsp(t_atof_tilde *x, t_signal **sp)
{
        dsp_add(atof_tilde_perform, 4, x,
                sp[0]->s_vec, sp[1]->s_vec, sp[0]->s_n);
}

void atof_tilde_free(t_atof_tilde *x)
{
        outlet_free(x->x_out);
}

void atof_tilde_setup(void)
{
        atof_tilde_class = class_new(gensym("atof~"),
                                     (t_newmethod)atof_tilde_new,
                                     (t_method)atof_tilde_free,
                                     sizeof(t_atof_tilde),
                                     CLASS_DEFAULT, 0);
        CLASS_MAINSIGNALIN(atof_tilde_class, t_atof_tilde, x_f);
        class_addmethod(atof_tilde_class, (t_method)atof_tilde_dsp,
                        gensym("dsp"), 0);
}
