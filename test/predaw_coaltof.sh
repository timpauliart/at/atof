inputname="coaltof.wav"

ffmpeg -i $inputname -filter_complex \
"[0:a]pan=mono|c0=c0[a0]; [0:a]pan=mono|c0=c1[a1]; [0:a]pan=mono|c0=c2[a2]; [0:a]pan=mono|c0=c3[a3]" \
-map "[a0]" -acodec pcm_s32le 1$inputname \
-map "[a1]" -acodec pcm_s32le 2$inputname \
-map "[a2]" -acodec pcm_s32le 3$inputname \
-map "[a3]" -acodec pcm_s32le 4$inputname 	
